//
//  PSYItemStore.h
//  Homepwner
//
//  Created by Robert Sogomonian on 4/7/14.
//  Copyright (c) 2014 Psyjnir, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PSYItem;

@interface PSYItemStore : NSObject

@property (nonatomic, readonly, copy) NSArray *allItems;

+ (instancetype)sharedStore;
- (PSYItem *)createItem;
- (void)removeItem:(PSYItem *)item;
- (void)moveItemAtIndex:(NSUInteger)fromIndex
                toIndex:(NSUInteger)toIndex;

@end
