Homepwner.Xcode
===============

Simple iOS App Developed Learning Objective-C

Following the book "iOS Programming - The Big Nerd Ranch Guide, 4th Edition"
This app is an iOS app that will eventually allow users to catalog all items in their 
house so that in the unfortunate case that something bad happens (fire, earthquake,
meteor, wrath of God, etc.) they have a handy list of items with pictures to show to
their insurance company.

This version of the app is an identical copy of what the book presents and will be
updated as I work through it. There is a second copy of this app here on github called
Homepwner.Xamarin which is my attempt to port the functionality in its entirety over
to C# using MonoTouch/Xamarin Studio.

I am attempting to write both versions simultaneously so that I can both learn the
Cocoa Touch frameworks (and know how to program applications for iPhone) and also see
how/if they translate as seamlessly to Xamarin/Mono Touch as their evangelists would
have us believe.

Eventually, if all goes well, the Homepwner.Xamarin version of the app will also be 
extended to include Android and Windows Phone support through Xamarin Studio.
