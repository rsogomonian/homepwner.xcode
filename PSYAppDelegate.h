//
//  PSYAppDelegate.h
//  Homepwner
//
//  Created by Robert Sogomonian on 4/7/14.
//  Copyright (c) 2014 Psyjnir, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSYAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
