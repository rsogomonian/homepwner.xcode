//
//  main.m
//  Homepwner
//
//  Created by Robert Sogomonian on 4/7/14.
//  Copyright (c) 2014 Psyjnir, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PSYAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PSYAppDelegate class]));
    }
}
