//
//  PSYItem.h
//  RandomItems
//
//  Created by Robert Sogomonian on 4/3/14.
//  Copyright (c) 2014 Psyjnir, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSYItem : NSObject

// -- Class Properties
@property (nonatomic, copy) NSString *itemName;
@property (nonatomic, copy) NSString *serialNumber;
@property (nonatomic) int valueInDollars;
@property (nonatomic, readonly, strong) NSDate *dateCreated;

// -- Class Methods
+ (instancetype)randomItem;

// -- Instance Methods
// -- Designated initializer for PSYItem
- (instancetype)initWithItemName:(NSString *)name
                  valueInDollars:(int)value
                    serialNumber:(NSString *)sNumber;

- (instancetype)initWithItemName:(NSString *)name
                    serialNumber:(NSString *)sNumber;

- (instancetype)initWithItemName:(NSString *)name;

@end
