//
//  PSYItemStore.m
//  Homepwner
//
//  Created by Robert Sogomonian on 4/7/14.
//  Copyright (c) 2014 Psyjnir, Inc. All rights reserved.
//

#import "PSYItemStore.h"
#import "PSYItem.h"

@interface PSYItemStore ()

@property (nonatomic) NSMutableArray *privateItems;

@end

@implementation PSYItemStore

+ (instancetype)sharedStore
{
    static PSYItemStore *sharedStore;
    
    // Do I need to create a new sharedStore?
    if (!sharedStore) {
        sharedStore = [[self alloc] initPrivate];
    }
    
    return sharedStore;
}

// If a programmer calls [[PSYItemStore alloc] init] let them
// know the error of their ways (and to call sharedStore instead)
- (instancetype)init
{
    [NSException raise:@"Singleton" format:@"Use +[PSYItemStore sharedStore]"];
    return nil;
}

// Here is the real (secret) initializer
- (instancetype)initPrivate
{
    self = [super init];
    
    if (self) {
        _privateItems = [[NSMutableArray alloc] init];
        [_privateItems addObject:@"No more items!"];
    }
    
    return self;
}

- (NSArray *)allItems
{
    return [self.privateItems copy];
}

- (PSYItem *)createItem
{
    PSYItem *item = [PSYItem randomItem];
    
    [self.privateItems insertObject:item atIndex:[self.privateItems count] - 1];
    
    return item;
}

- (void)removeItem:(PSYItem *)item
{
    [self.privateItems removeObjectIdenticalTo:item];
}

- (void)moveItemAtIndex:(NSUInteger)fromIndex toIndex:(NSUInteger)toIndex
{
    if (fromIndex == toIndex) {
        return;
    }
    
    PSYItem *item = self.privateItems[fromIndex];
    [self.privateItems removeObjectAtIndex:fromIndex];
    [self.privateItems insertObject:item atIndex:toIndex];
}

@end
