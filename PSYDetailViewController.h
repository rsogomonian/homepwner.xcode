//
//  PSYDetailViewController.h
//  Homepwner
//
//  Created by Robert Sogomonian on 4/9/14.
//  Copyright (c) 2014 Psyjnir, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PSYItem;

@interface PSYDetailViewController : UIViewController

@property (nonatomic, strong) PSYItem *item;

@end
